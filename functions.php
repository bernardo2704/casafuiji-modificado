<?php
/**
 * Electro Child
 *
 * @package electro-child
 */

/**
 * Include all your custom code here
 */

function dinamo_scripts() {
    wp_enqueue_script(
        'dinamo-script',
        get_stylesheet_directory_uri() . '/scripts.js',
        array( 'jquery' )
    );
}

add_action( 'wp_enqueue_scripts', 'dinamo_scripts' );