/*
 Theme Name:    Electro Child
 Theme URI:     https://themeforest.net/item/electro-electronics-store-woocommerce-theme/15720624
 Description:   This is the child theme of Electro
 Author:        MadrasThemes
 Author URI:    https://madrasthemes.com/
 Template:      electro
 Version:       2.3.3
 License:       GNU General Public License v2 or later
 License URI:   http://www.gnu.org/licenses/gpl-2.0.html
 Tags:          light, dark, two-columns, right-sidebar, responsive-layout
 Text Domain:   electro-child
*/


// function recreateMenu() {

	//  esto es un parche por si el cliente quiere otra cosa
	
	// var dropdownMenu = jQuery('.dropdown-menu').first();
	// var dropdownMenus = jQuery('#menu-nav-principal-1 .dropdown-menu');
	// var menuProductosCustom = dropdownMenu.clone(false);
	// var mainNavContainer =  jQuery('#main-nav');
	// var procuctToggler = jQuery('.dropdown-toggle');

	// // clono el submenu de eventos
	// mainNavContainer.html(menuProductosCustom);
	// console.log('cloned')
	// // lo elimino de donde estaba
	// //dropdownMenus.remove();
	// console.log('deleted')

	// //lo activo con un hover
	// procuctToggler.mouseover(function(){
	// 	jQuery('#main-nav>.dropdown-menu').css({'display':'block', 'position': 'relative','top':'auto'});
	// }).mouseout(function(){
	// 	setTimeout(function () {
 //        	jQuery('#main-nav>.dropdown-menu').css({'display':'none', 'position': 'absolute','top':'100%'});
 //    	}, 3000);
		
	// });
	// mainNavContainer.mouseover(function(){
	// 	jQuery('#main-nav>.dropdown-menu').css({'display':'block', 'position': 'relative','top':'auto'});
	// }).mouseout(function(){
	// 	setTimeout(function () {
	// 		jQuery('#main-nav>.dropdown-menu').css({'display':'none', 'position': 'absolute','top':'100%'});
	// 	}, 3000);
		
	// });

	

// }

// jQuery().ready(recreateMenu)

                                               


	//selectores generales
	var iconContainer = '#masthead > div.container.hidden-lg-down > div.masthead > div.header-icons';
	var iconRastrear = `<div class="div.masthead div.header-icons i"><a href="/rastrear-orden"><i class="fa fa-truck" style="font-size: 20px;" ><span style="display:inline-block; margin-left: 5px">Rastrear<span>`
	var iconFavs = `#masthead > div.container.hidden-lg-down > div.masthead > div.header-icons > div:nth-child(1) > a`;
	var iconAccount =`#masthead > div.container.hidden-lg-down > div.masthead > div.header-icons > div.header-icon.dropdown.animate-dropdown > a`;

	var dropdownToggler = `#masthead > div.container.hidden-lg-down > div.electro-navigation.yes-home > div.departments-menu-v2 > div > a`;
	
	function customizeHeader(){
	// esto es para anadir etiquetas y el icono que faltaba
		jQuery(iconContainer).append(iconRastrear);
		jQuery(iconFavs).append(`<span style="display:inline-block; margin-left: 5px">Wishlist<span>`);
		jQuery(iconAccount).append(`<span style="display:inline-block; margin-left: 5px">Mi cuenta<span>`)
		
		
	//esto le anade un fondo al menu de productos
		jQuery(dropdownToggler).on('click', function(){
			var dropdown = 'ul#menu-prueba-megamenu-mark-1.dropdown-menu.yamm';
			//var dropdown = '.dropdown-menu.yamm';
			var parentHeight = jQuery(dropdown).outerHeight();
			// TODO: Bernie mueve estos estilos a un mejor lugar
			jQuery(dropdown).prepend(`<div class="background-megamenu"><div>`)
			// TODO: Bernie, buscar el estido que le da una sombra a los submenues par quitarselas
		});

	}
	
	// la funcion ready se activa cuando la pagina esta lista
	jQuery().ready(customizeHeader)

